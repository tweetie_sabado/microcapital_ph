<?php

function lenard_preprocess_html(&$variables)
{
  // Add variables for path to theme.
  $variables['base_path'] = base_path();
  $variables['path_to_theme'] = drupal_get_path('theme', 'lenard');
}

/**
 * Unset core and contrib JS and CSS files.
 */
function lenard_css_alter(&$css) {
  // Unset Drupal core CSS files to avoid conflicts; we'll style these in the main stylesheets
  unset($css[drupal_get_path('module', 'system') . '/system.menus.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.messages.css']);
  unset($css[drupal_get_path('module', 'system') . '/system.theme.css']);
  unset($css[drupal_get_path('module', 'user') . '/user.css']);
  unset($css[drupal_get_path('module', 'field') . '/theme/field.css']);
}

/**
 * Override or insert variables into the page template for specific content types.
 */
function lenard_preprocess_page(&$variables) {
  if (isset($variables['node']->type)) {
    $nodetype = $variables['node']->type;
    $variables['theme_hook_suggestions'][] = 'page__' . $nodetype;
  }

  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/cocoon-preprocess.js'), array('type' => 'file', 'scope' => 'header', 'weight' => 100));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/jquery.themepunch.tools.min.js'), array('type' => 'file', 'scope' => 'header', 'weight' => 101));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/jquery.themepunch.revolution.min.js'), array('type' => 'file', 'scope' => 'header', 'weight' => 102));
  drupal_add_js('http://maps.googleapis.com/maps/api/js?sensor=false', array('type' => 'external', 'scope' => 'footer', 'weight' => 10));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/bootstrap.min.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 4));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/parallax.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 5));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/retina.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 6));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/bxslider.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 7));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/circle-progress.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 8));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/isotope.pkgd.min.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 9));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/jquery.prettyPhoto.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 12));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/wow.min.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 14));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/smooth-scroll.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 15));
    $map_latitude = theme_get_setting('map_latitude');
    $map_longitude = theme_get_setting('map_longitude');
    $map_marker = theme_get_setting('map_marker');
    $map_marker_title = theme_get_setting('map_marker_title');
    $map_marker_title_link = theme_get_setting('map_marker_title_link');
    $map_marker_address = theme_get_setting('map_marker_address');
    $map_marker_additional_information = theme_get_setting('map_marker_additional_information');
  drupal_add_js('
function initWorkFilter() {
    ! function(e) {
        "use strict";
        var t;
        t = work_grid.hasClass("masonry") ? "masonry" : "fitRows", work_grid.imagesLoaded(function() {
            work_grid.isotope({
                itemSelector: ".mix",
                layoutMode: t,
                filter: fselector
            })
        }), e(".filter").click(function() {
            return e(".filter").removeClass("active"), e(this).addClass("active"), fselector = e(this).attr("data-filter"), work_grid.isotope({
                itemSelector: ".mix",
                layoutMode: t,
                filter: fselector
            }), !1
        })
    }(jQuery)
}

function init_masonry() {
    ! function(e) {
        e(".masonry").imagesLoaded(function() {
            e(".masonry").masonry()
        })
    }(jQuery)
}! function(e) {
    "use strict";
    e(window).load(function() {
        e("#loader").delay(600).fadeOut("slow"), e("#loader-container").delay(600).fadeOut("slow"), e("body").delay(600).css({
            overflow: "visible"
        })
    }), e(function() {
        function t(e) {
            var o = parseInt(e.html(), 10);
            e.html(++o), o !== e.data("count") && setTimeout(function() {
                t(e)
            }, 10)
        }
        e(".stat-count").each(function() {
            e(this).data("count", parseInt(e(this).html(), 10)), e(this).html("0"), t(e(this))
        })
    }), e(".bxslider").bxSlider({
        mode: "vertical",
        minSlides: 1,
        maxSlides: 1,
        slideMargin: 0,
        pager: !1,
        nextText: \'<i class="fa fa-arrow-down"></i>\',
        prevText: \'<i class="fa fa-arrow-up"></i>\',
        speed: 1e3,
        auto: !0
    }), e(function() {
        e.stellar({
            horizontalScrolling: !1,
            verticalOffset: 40
        })
    }), e(".circle05").circleProgress({
        value: .025,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle10").circleProgress({
        value: .05,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle15").circleProgress({
        value: .075,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle20").circleProgress({
        value: .1,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle25").circleProgress({
        value: .125,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle30").circleProgress({
        value: .15,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle35").circleProgress({
        value: .175,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle40").circleProgress({
        value: .2,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle45").circleProgress({
        value: .225,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle50").circleProgress({
        value: .25,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle55").circleProgress({
        value: .275,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle60").circleProgress({
        value: .3,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle65").circleProgress({
        value: .325,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle70").circleProgress({
        value: .35,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle75").circleProgress({
        value: .375,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle80").circleProgress({
        value: .4,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle85").circleProgress({
        value: .425,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle90").circleProgress({
        value: .45,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle95").circleProgress({
        value: .475,
        size: 240,
        fill: {
            color: "#d90000"
        }
    }), e(".circle100").circleProgress({
        value: .5,
        size: 240,
        fill: {
            color: "#d90000"
        }
    });
    var t, o, i = [
            [\'<div class="infobox"><h3 class="title"><a target="_blank" href="'.$map_marker_title_link.'">'.$map_marker_title.'</a></h3><span>'.$map_marker_address.'</span><br>'.$map_marker_additional_information.'</p></div></div></div>\', -37.801578, 145.060508, 2]
        ],
        a = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            scrollwheel: !1,
            navigationControl: !0,
            mapTypeControl: !1,
            scaleControl: !1,
            draggable: !0,
            styles: [{
                stylers: [{
                    hue: "#000"
                }, {
                    saturation: -100
                }, {
                    gamma: 1.6
                }]
            }],
            center: new google.maps.LatLng('.$map_latitude.', '.$map_longitude.'),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        }),
        n = new google.maps.InfoWindow;
    for (o = 0; o < i.length; o++) t = new google.maps.Marker({
        position: new google.maps.LatLng(i[o][1], i[o][2]),
        map: a,
        icon: "'.$map_marker.'"
    }), google.maps.event.addListener(t, "click", function(e, t) {
        return function() {
            n.setContent(i[t][0]), n.open(a, e)
        }
    }(t, o));
    e("#testimonials").length && (e(".testimonial-nav").each(function() {
        var t = e(this),
            o = t.children("li");
        o.sort(function() {
            var e = parseInt(8 * Math.random()),
                t = e % 2,
                o = e > 5 ? 1 : -1;
            return t * o
        }).appendTo(t)
    }), e("#testimonials .testimonial:eq(8),#testimonials .testimonial-nav a:eq(8)").addClass("active"), e("#testimonials .testimonial-nav a").hover(function() {
        e("#testimonials .testimonial-nav a,#testimonials .testimonial").removeClass("active"), e(this).addClass("active"), e(e(this).attr("href")).addClass("active")
    }), e("#testimonials .testimonial-nav a").click(function() {
        return !1
    })), jQuery("a[data-gal]").each(function() {
        jQuery(this).attr("rel", jQuery(this).data("gal"))
    }), jQuery("a[data-gal^=\'prettyPhoto\']").prettyPhoto({
        animationSpeed: "slow",
        slideshow: !1,
        overlay_gallery: !1,
        theme: "light_square",
        social_tools: !1,
        deeplinking: !1
    }), new WOW({
        offset: 120
    }).init(), e(".header").affix({
        offset: {
            top: e(".header").height()
        }
    }), smoothScroll.init({
        speed: 1e3,
        easing: "easeInOutCubic",
        offset: 70,
        updateURL: !1,
        callbackBefore: function() {},
        callbackAfter: function() {}
    })
}(jQuery), jQuery(document).ready(function() {
    jQuery(".tp-banner").show().revolution({
        dottedOverlay: "none",
        delay: 16e3,
        startwidth: 1140,
        startheight: 850,
        hideThumbs: 200,
        thumbWidth: 100,
        thumbHeight: 50,
        thumbAmount: 4,
        navigationType: "none",
        navigationArrows: "solo",
        navigationStyle: "preview1",
        touchenabled: "on",
        onHoverStop: "on",
        swipe_velocity: .7,
        swipe_min_touches: 1,
        swipe_max_touches: 1,
        drag_block_vertical: !1,
        parallax: "scroll",
        parallaxBgFreeze: "on",
        parallaxLevels: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
        keyboardNavigation: "off",
        navigationHAlign: "center",
        navigationVAlign: "bottom",
        navigationHOffset: 0,
        navigationVOffset: 20,
        soloArrowLeftHalign: "left",
        soloArrowLeftValign: "center",
        soloArrowLeftHOffset: 20,
        soloArrowLeftVOffset: 0,
        soloArrowRightHalign: "right",
        soloArrowRightValign: "center",
        soloArrowRightHOffset: 20,
        soloArrowRightVOffset: 0,
        shadow: 0,
        fullWidth: "on",
        fullScreen: "on",
        spinner: "spinner4",
        stopLoop: "off",
        stopAfterLoops: -1,
        stopAtSlide: -1,
        shuffle: "off",
        autoHeight: "off",
        forceFullWidth: "on",
        hideThumbsOnMobile: "off",
        hideNavDelayOnMobile: 1500,
        hideBulletsOnMobile: "off",
        hideArrowsOnMobile: "off",
        hideThumbsUnderResolution: 0,
        hideSliderAtLimit: 0,
        hideCaptionAtLimit: 0,
        hideAllCaptionAtLilmit: 0,
        startWithSlide: 0
    })
}),
function(e) {
    "use strict";
    e(document).ready(function() {
        e(window).trigger("resize"), initWorkFilter(), init_masonry()
    })
}(jQuery);
var fselector = 0,
    work_grid = jQuery("#masonry_grid");
               ',
                 array('type' => 'inline', 'scope' => 'footer', 'weight' => 18)); //end inline drupal_add_js jquery.custom.js
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/owl.carousel.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 19));
  drupal_add_js((drupal_get_path('theme', 'lenard') . '/js/owl.scripts.js'), array('type' => 'file', 'scope' => 'footer', 'weight' => 20));

}

/**
 * Override or insert variables into the node template.
 */
function lenard_preprocess_node(&$variables)
{
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

/**
 * Override or insert variables into the block template.
 */
function lenard_preprocess_block(&$variables)
{
  // In the header region visually hide block titles.
  if ($variables['block']->region == 'header') {
    $variables['title_attributes_array']['class'][] = 'element-invisible';
  }
}

/**
 * Implements theme_field__field_type().
 */
function lenard_field__taxonomy_term_reference($variables)
{
  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . ': </h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * Implements theme_menu_tree().
 */
function lenard_menu_tree($variables)
{
  return '<ul class="menu clearfix">' . $variables['tree'] . '</ul>';
}

// Disabling the default system main menu
function lenard_links__system_main_menu() {
  return null;
}

// main-menu: Adding classes to main <ul>
function lenard_menu_tree__main_menu($variables) {
  return '<ul class="nav navbar-nav navbar-right">'.$variables['tree'].'</ul>';
}

// main-menu: Adding classes to dropdown <ul>
function lenard_menu_tree__main_menu_inner($variables) {
    return '<ul class="dropdown-menu" role="menu">' . $variables['tree'] . '</ul>';
}

// main-menu: Theming menu links
function lenard_menu_link__main_menu(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';
  $element['#localized_options']['attributes']['data-scroll'][] = '';

  if ($element['#below']) {
    foreach ($element['#below'] as $key => $val) {
      if (is_numeric($key)) {             
        $element['#below'][$key]['#theme'] = 'menu_link__main_menu_inner'; // Second level <li>
      }
    }
    $element['#below']['#theme_wrappers'][0] = 'menu_tree__main_menu_inner';  // Second level <ul>
    $sub_menu = drupal_render($element['#below']);
    $output = l($element['#title'], $element['#href'], $element['#localized_options']);
    return '<li' . drupal_attributes($element['#attributes']) . '><span class="dropdown-toggle" data-toggle="dropdown">' . $output . '<b class="caret"></b></span>' . $sub_menu . "</li>\n";
  }
  $output = l($element['#title'], $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

// main-menu: Theming inner menu links
function lenard_menu_link__main_menu_inner(array $variables) {
  $element = $variables['element'];
  $sub_menu = '';

  if ($element['#below']) {
    $sub_menu = drupal_render($element['#below']);
  }

  $element['#localized_options']['html'] = TRUE;
  $linktext = '<span>' . $element['#title'] . '</span>';

  $output = l($linktext, $element['#href'], $element['#localized_options']);
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
}

/**
 * Form alter
 */
function lenard_form_alter(&$form, &$form_state, $form_id)
{
  // Main contact form
  if ($form_id == 'contact_site_form') {
    $form['#attributes'] = array('class' => 'col-md-6 col-md-offset-3 text-center wow fadeInUp animated');
    $form['name']['#attributes']['placeholder'] = t('Name');
    $form['mail']['#attributes']['placeholder'] = t('Email');
    $form['subject']['#attributes']['placeholder'] = t('Subject');
    $form['message']['#attributes']['placeholder'] = t('Message');
    unset($form['name']['#title']);
    unset($form['mail']['#title']);
    unset($form['subject']['#title']);
    unset($form['message']['#title']);
    $form['message']['#resizable'] = FALSE;
  }
  // Search page form
  if ($form_id == 'search_form') {
    // Adding placeholders to fields
    $form['basic']['keys']['#attributes']['placeholder'] = t('Enter some terms...');
    $form['advanced']['keywords']['or']['#attributes']['placeholder'] = t('e.g. bike, shed');
    $form['advanced']['keywords']['phrase']['#attributes']['placeholder'] = t('e.g. outside the bike shed');
    $form['advanced']['keywords']['negative']['#attributes']['placeholder'] = t('e.g. bicycle');
    unset($form['basic']['keys']['#title']);
  }
  // Search form block 
  if ($form_id == 'search_block_form') {
    $form['#attributes']['class'][] = 'stylish-input-group';
    $form['search_block_form']['#attributes']['placeholder'] = t('What are you looking for?');
    $form['search_block_form']['#attributes']['class'][] = 'form-control search';

    // Hide main search button, but don't unset
    hide($form['actions']['submit']);

    $form['button'] = array(
      '#prefix' => '<span class="input-group-addon"><button type="submit" id="edit-submit" name="op"><span class="glyphicon glyphicon-search">',
      '#suffix' => '</span></button></span>',
      '#markup' => '', // Required to force the element to render
      '#weight' => 1000,
    );
  }
}

// Theming comment form
function lenard_form_comment_form_alter(&$form, &$form_state) {
  $form['author']['name']['#attributes']['placeholder'] = t('Name');
  $form['author']['mail']['#attributes']['placeholder'] = t('Email (Will be kept private)');
  $form['subject']['#attributes']['placeholder'] = t('Subject');
  $form['comment_body'][$form['comment_body']['#language']][0]['#attributes']['placeholder'] = t('Comment');
  $form['comment_body'][$form['comment_body']['#language']][0]['#resizable'] = false;
  $form['comment_body']['#after_build'][] = 'lenard_configure_comment_form';
  $form['actions']['submit']['#attributes']['value'][] = t('Submit');
  $form['actions']['submit']['#attributes']['class'][] = 'btn-primary uppercase';
  $form['actions']['cancel']['#attributes']['class'][] = 'btn-primary uppercase';
  unset($form['actions']['preview']);
  $form['author']['homepage']['#access'] = FALSE;
}

function lenard_configure_comment_form(&$form) {
  unset($form[LANGUAGE_NONE][0]['format']);
  return $form;
}

/* Theming all submit buttons */
function lenard_button($variables) {
  $element = $variables ['element'];
  $element ['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));
  $element ['#attributes']['class'][] = 'btn btn-primary submit';
  if (!empty($element ['#attributes']['disabled'])) {
    $element ['#attributes']['class'][] = 'form-button-disabled';
  }
  return '<input' . drupal_attributes($element ['#attributes']) . ' />';
}

/* Pager */
function lenard_pager_link($variables) {
  $text = $variables['text'];
  $page_new = $variables['page_new'];
  $element = $variables['element'];
  $parameters = $variables['parameters'];
  $attributes = $variables['attributes'];

  $page = isset($_GET['page']) ? $_GET['page'] : '';
  if ($new_page = implode(',', pager_load_array($page_new[$element], $element, explode(',', $page)))) {
    $parameters['page'] = $new_page;
  }

  $query = array();
  if (count($parameters)) {
    $query = drupal_get_query_parameters($parameters, array());
  }
  if ($query_pager = pager_get_query_parameters()) {
    $query = array_merge($query, $query_pager);
  }

  // Set each pager link title
  if (!isset($attributes['title'])) {
    static $titles = NULL;
    if (!isset($titles)) {
      $titles = array(
        t('« first') => t('Go to first page'),
        t('‹ previous') => t('Go to previous page'),
        t('next ›') => t('Go to next page'),
        t('last »') => t('Go to last page'),
      );
    }
    if (isset($titles[$text])) {
      $attributes['title'] = $titles[$text];
    }
    elseif (is_numeric($text)) {
      $attributes['title'] = t('Go to page @number', array('@number' => $text));
    }
  }

  // @todo l() cannot be used here, since it adds an 'active' class based on the
  //   path only (which is always the current path for pager links). Apparently,
  //   none of the pager links is active at any time - but it should still be
  //   possible to use l() here.
  // @see http://drupal.org/node/1410574
  $attributes['href'] = url($_GET['q'], array('query' => $query));
  return '<a' . drupal_attributes($attributes) . '>' . $text . '</a>';
}

/* Messages */
function lenard_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );

  // Mapping default Drupal message statuses to Bootstrap statuses
  $bootstrap_statuses = array(
    'status' => 'alert-success',
    'error' => 'alert-warning',
    'warning' => 'alert-danger',
  );

  foreach (drupal_get_messages($display) as $type => $messages) {
    // Grab class for Bootstrap status matching its $type
    $bootstrap_class = $bootstrap_statuses[$type];
    $output .= "<div class=\"fade-in alert $bootstrap_class\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">×</button>\n";
    if (!empty($status_heading[$type])) {
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
    }
    if (count($messages) > 1) {
      $output .= " <ul>\n";
      foreach ($messages as $message) {
        $output .= '  <li>' . $message . "</li>\n";
      }
      $output .= " </ul>\n";
    }
    else {
      $output .= $messages[0];
    }
    $output .= "</div>\n";
  }
  return $output;
}

/* System Tabs */
function lenard_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="tabs primary nav nav-pills">';
    $variables['primary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary nav nav-pills">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }

  return $output;
}


function lenard_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Adding the title of the current page to the breadcrumb.
    $breadcrumb[] = drupal_get_title();
    
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';

    $output .= '<div class="breadcrumb pull-right">' . implode('&nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;', $breadcrumb) . '</div>';
    return $output;
  }
}
