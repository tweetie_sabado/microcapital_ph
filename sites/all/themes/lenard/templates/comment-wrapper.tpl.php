<a id="comments-section"></a>
<div id="comments-wrapper" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php if ($content['comments'] && $node->type != 'forum'): ?>
    <?php print render($title_prefix); ?>
    <div class="widget-title">
      <h3><?php print 'Comments <span>(' . $node->comment_count . ')</span>'; ?></h3>
      <div class="border-normal"></div>
    </div>
    <?php print render($title_suffix); ?>
  <?php endif; ?>

  <ul id="comments" class="media-list comments margin-top comment-wrapper comment-wrapper-nid-<?php print $node->nid?> comments-list"><?php print render($content['comments']); ?></ul>
  <div class="clearfix"></div>
  <br>
  <?php if ($content['comment_form']): ?>
    <div class="widget-title">
      <h3 class="comments-title"><?php print t('Leave a comment'); ?></h3>
    </div>
    <div class="margin-top">
      <?php print render($content['comment_form']); ?>
    </div>
  <?php endif; ?>
</div>