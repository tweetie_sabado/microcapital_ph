<div id="<?php print $block_html_id; ?>" class="<?php print $classes; ?> widget"<?php print $attributes; ?>>

  <?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
  <div class="widget-title">
    <h3<?php print $title_attributes; ?>><?php print $block->subject ?></h3>
    <div class="border-normal"></div>
  </div>
<?php endif;?>
  <?php print render($title_suffix); ?>

  <div class="content"<?php print $content_attributes; ?>>
    <?php print $content ?>
  </div>
</div>
