<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div<?php if ($classes_array[$id]) { print ' id="' . $classes_array[$id] .'"';  } ?> class="tab-pane">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
