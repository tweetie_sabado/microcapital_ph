<?php
  $base_path = base_path();
  $path_to_theme = drupal_get_path('theme', 'lenard');
?>


<div id="opensearch" class="collapse myform">
		<div class="container">
            <?php print render($page['header_search']); ?>
		</div>
	</div>


<header class="header affix-top">
     	<div class="topbar">
        	<div class="container">
        <?php if ($page['social_menu']): ?>
<div class="social text-right">
            <?php print render($page['social_menu']); ?>
</div>
        <?php endif; ?>
        	</div><!-- end container -->
        </div><!-- end topbar -->

		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <i class="fa fa-bars"></i>
                    </button>
                    <a data-scroll class="navbar-brand" href="#home"><img src="<?php print $logo; ?>" alt=""></a>
				</div>
        <?php if ($page['main_menu']): ?>
          <div id="navbar-collapse" class="navbar-collapse collapse">
            <?php print render($page['main_menu']); ?>
          </div><!-- /.nav-collapse -->
        <?php endif; ?>
			</div><!--/.container-fluid -->
		</nav>

</header><!-- /.header -->


<?php if ($page['hero']): ?>
	<article id="home" class="slider-wrapper">
				<?php print render($page['hero']); ?>
	</article>
<?php endif; ?>

<div class="drupal-console">
  <a id="main-content"></a>            
  <section>
    <?php if ($messages): ?>
    <div id="messages"><div class="container">
      <?php print $messages; ?>
    </div></div> <!-- /.container, /#messages -->
    <?php endif; ?>
  </section>
  <?php print render($page['help']); ?>
  <?php if ($action_links): ?>
    <ul class="action-links">
      <?php print render($action_links); ?>
    </ul>
  <?php endif; ?>
</div><!-- /.drupal-console -->

    <section id="about" class="section white-section">
       <!-- <div class="grey-shape"> <div class="icon-container fixed border-radius"><i class="fa fa-check"></i></div></div> -->
        <div class="section-container">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="service-text">
<?php if ($page['onepage_about_info']): ?>
				<?php print render($page['onepage_about_info']); ?>
<?php endif; ?>
                            <div class="border-title fixleftborder"></div>
                        </div><!-- end service-box -->
                    </div><!-- end col -->

<?php if ($page['onepage_about_blocks']): ?>
				<?php print render($page['onepage_about_blocks']); ?>
<?php endif; ?>

                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->


<?php if ($page['onepage_textslider']): ?>
    <section class="section red-section nopadding">
        <div class="row-fluid">
            <div class="col-md-6 pull-right myimg"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="section-container nopadding">
				<?php print render($page['onepage_textslider']); ?>
                        </div>
                    </div>
                </div><!--end of row-->
            </div><!-- end container -->
        </div><!-- end row fluid -->
    </section>
<?php endif; ?>



    <section class="section grey-section">
     <!--   <div class="white-shape"><div class="icon-container fixed border-radius"><i class="fa fa-user"></i></div></div> -->
        <div class="section-container">
            <div class="container">
                <div class="big-title">
                    <h3><span>More</span> About Us</h3>
                    <div class="border-title"></div>
                </div><!-- big-title -->
<?php if ($page['onepage_more_about_blocks']): ?>
				<?php print render($page['onepage_more_about_blocks']); ?>
<?php endif; ?>
            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->

    <section id="features" class="section white-section">
        <div class="section-container">
            <div class="container"> 
            	<div class="row">
                	<div class="col-md-5 features-widget">

<?php if ($page['onepage_features_tabs']): ?>
				<?php print render($page['onepage_features_tabs']); ?>
<?php endif; ?>

                    </div><!-- end col -->
                    
                    <div class="col-md-7">

<?php if ($page['onepage_features_content']): ?>
				<?php print render($page['onepage_features_content']); ?>
<?php endif; ?>

                    </div><!-- end col -->
      			</div><!-- end row -->
            </div><!-- end container -->
    	</div><!-- end section-container -->           
    </section><!-- end section -->

	<section id="parallax1" class="section-parallax" data-stellar-background-ratio="0.1" data-stellar-vertical-offset="1">
    	<div class="white-shape leftalign"></div>
    	<div class="section-container parallax-wrapper">
        	<div class="container">
<?php if ($page['onepage_facts_counter']): ?>
				<?php print render($page['onepage_facts_counter']); ?>
<?php endif; ?>
        	</div><!-- end container -->
		</div><!-- end section-container -->
    </section><!-- end section -->

    <section id="services" class="section grey-section">
    <!--    <div class="white-shape"><div class="icon-container fixed border-radius"><i class="fa fa-cog"></i></div></div> -->
        <div class="section-container">
            <div class="container">

<?php if ($page['onepage_what_we_do']): ?>
				<?php print render($page['onepage_what_we_do']); ?>
<?php endif; ?>
            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->

    <section class="section white-section">
        <div class="section-container">
            <div class="container">

<?php if ($page['onepage_skills']): ?>
				<?php print render($page['onepage_skills']); ?>
<?php endif; ?>


        
            </div><!-- end container -->
    	</div><!-- end section-container -->           
    </section><!-- end section -->
 
	<section id="parallax2" class="section-parallax" data-stellar-background-ratio="0.1" data-stellar-vertical-offset="0">
    	<div class="white-shape rightbottomalign"></div>
    	<div class="parallax-wrapper">
        	<div class="container">
<?php if ($page['onepage_testimonials']): ?>
				<?php print render($page['onepage_testimonials']); ?>
<?php endif; ?>

        	</div><!-- end container -->
		</div><!-- end section-container -->
    </section><!-- end section -->

    <section id="works" class="section white-section paddingbottom">
    <!--    <div class="grey-shape"><div class="icon-container fixed border-radius"><i class="fa fa-paint-brush"></i></div></div> -->
        <div class="section-container paddingbottom">

<?php if ($page['onepage_portfolio']): ?>
				<?php print render($page['onepage_portfolio']); ?>
<?php endif; ?>


        </div><!-- end section-container -->
    </section><!-- end section -->

    <section id="pricing" class="section white-section paddingbottom">
   <!--     <div class="grey-shape"><div class="icon-container fixed border-radius"><i class="fa fa-sitemap"></i></div></div> -->
        <div class="section-container paddingbottom">
            <div class="container">

<?php if ($page['onepage_pricing']): ?>
				<?php print render($page['onepage_pricing']); ?>
<?php endif; ?>
                
            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->

<?php if ($page['onepage_homepage_video']): ?>
				<?php print render($page['onepage_homepage_video']); ?>
<?php endif; ?>


    <section id="team" class="section grey-section">
      <!--  <div class="white-shape"><div class="icon-container fixed border-radius"><i class="fa fa-users"></i></div></div> -->
        <div class="section-container">
            <div class="container">
<?php if ($page['onepage_team']): ?>
				<?php print render($page['onepage_team']); ?>
<?php endif; ?>


            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->

    <section class="section white-section">
        <div class="section-container">
            <div class="container">
<?php if ($page['onepage_clients']): ?>
				<?php print render($page['onepage_clients']); ?>
<?php endif; ?>

            </div><!-- end container -->
    	</div><!-- end section-container -->           
    </section><!-- end section -->
    
	<article id="contact" class="map-section">
		<div id="map" class="wow slideInUp<?php if (theme_get_setting('show_map') == 0): ?> hide<?php endif; ?>"></div>
	</article><!-- end section -->
    
    <section class="section white-section">
   <!--     <div class="grey-shape"><div class="icon-container fixed border-radius"><i class="fa fa-map-marker"></i></div></div> --></-->
        <div class="section-container">
            <div class="container">
                <div class="big-title">
<?php if ($page['onepage_contact_title']): ?>
				<?php print render($page['onepage_contact_title']); ?>
<?php endif; ?>

                    <div class="border-title"></div>
                </div><!-- big-title -->
                <div class="row contact-wrapper">
                	<div class="col-md-9 col-sm-9 col-xs-12">
                        <div id="contact_form" class="contact_form">
                            <div class="col-md-12">

<?php if ($page['onepage_contact_form']): ?>
				<?php print render($page['onepage_contact_form']); ?>
<?php endif; ?>

                            </div>
                        </div><!-- end contact-form -->
                    </div><!-- end col -->
                
                	<div class="col-md-3 col-sm-3 col-xs-12">
                    	<div class="contact-list">
<?php if ($page['onepage_contact_info']): ?>
				<?php print render($page['onepage_contact_info']); ?>
<?php endif; ?>

                    	</div><!-- end contact-list -->
                    </div><!-- end col -->
                </div><!-- end row -->
            </div><!-- end container -->
        </div><!-- end section-container -->
    </section><!-- end section -->
    
	<section id="parallax4" class="section-parallax" data-stellar-background-ratio="0.1" data-stellar-vertical-offset="1">
    	<div class="parallax-wrapper">
        	<div class="container">
<?php if ($page['social_links']): ?>
				<?php print render($page['social_links']); ?>
<?php endif; ?>

                                
                <div class="row copyrights">
<?php if ($page['copyright']): ?>
				<?php print render($page['copyright']); ?>
<?php endif; ?>
                </div><!-- end row -->
        	</div><!-- end container -->
		</div><!-- end section-container -->
    </section><!-- end section -->
