<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <li><a<?php if ($classes_array[$id]) { print ' href="#' . $classes_array[$id] .'"';  } ?>>
    <?php print $row; ?>
  </a></li>
<?php endforeach; ?>
