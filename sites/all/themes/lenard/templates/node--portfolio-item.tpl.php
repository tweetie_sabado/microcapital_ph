<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
<div id="post-wrapper">
  <?php print render($title_prefix); ?>
  <?php if (!$page): ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
  <?php endif; ?>
  <?php print render($title_suffix); ?>

<div class="portfoliostyle">
<div class="portfolio-wrapper">
<ul class="portfolio-container clearfix image-hover-red hover-titles-effect masonry" id="masonry_grid">

  <?php if (isset($content['field_content_image_1'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_1[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_1[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_1['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_1[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_1[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_1['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>

  <?php if (isset($content['field_content_image_2'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_2[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_2[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_2['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_2[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_2[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_2['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>
  
  <?php if (isset($content['field_content_image_3'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_3[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_3[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_3['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_3[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_3[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_3['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>

  <?php if (isset($content['field_content_image_4'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_4[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_4[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_4['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_4[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_4[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_4['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>

  <?php if (isset($content['field_content_image_5'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_5[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_5[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_5['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_5[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_5[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_5['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>

  <?php if (isset($content['field_content_image_6'])) { ?>
  <li class="portfolio-item mix">
    <div class="portfolio-image">
      <?php if (isset($node->field_content_image_6[$node->language])) { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_6[$node->language][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } else { ?>
        <img src="<?php print image_style_url('portfolio_main', $node->field_content_image_6['LANGUAGE_NONE'][0]['uri']); ?>" class="img-responsive" alt="">
      <?php } ?>
    </div>
    <div class="portfolio-hover-desc">
      <div class="portfolio-buttons">
        <a data-gal="prettyPhoto[product-gallery]" rel="prettyPhoto[product-gallery]" href="
          <?php if (isset($node->field_content_image_6[$node->language])) { ?>
            <?php print file_create_url($node->field_content_image_6[$node->language][0]['uri']); ?>
          <?php } else { ?>
            <?php print file_create_url($node->field_content_image_6['LANGUAGE_NONE'][0]['uri']); ?>
          <?php } ?>
        ">
          <i class="fa fa-search"></i>
        </a>
      </div>
    </div>
  </li><!-- /.portfolio-item -->
  <?php } ?>
</ul>
</div>
</div>

<div id="singleportfolio" class="row portfolio-wrapper text-center clearfix">
  <div class="col-md-8 col-md-offset-2 content"<?php print $content_attributes; ?>>

<div class="center-title text-center"><h3 class="section-mini-title uppercase"><?php print $title; ?></h3>
<div class="border-title"></div></div>
<?php print render($content['body']); ?>
<?php if (isset($content['field_date'])) { ?>
  <div class="portfolio-meta">
    <div class="portfolio-meta-inner">
      <p><?php print render($content['field_date']); ?></p>
    </div>
  </div>
<?php } ?>
<ul class="social text-center clearfix">
  <?php if (isset($content['field_behance_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_behance_url']); ?>"><i class="fa fa-behance"></i></a></li>
  <?php } ?>
  <?php if (isset($content['field_twitter_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_twitter_url']); ?>"><i class="fa fa-twitter"></i></a></li>
  <?php } ?>
  <?php if (isset($content['field_dribbble_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_dribbble_url']); ?>"><i class="fa fa-dribbble"></i></a></li>
  <?php } ?>
  <?php if (isset($content['field_linkedin_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_linkedin_url']); ?>"><i class="fa fa-linkedin"></i></a></li>
  <?php } ?>
  <?php if (isset($content['field_facebook_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_facebook_url']); ?>"><i class="fa fa-facebook"></i></a></li>
  <?php } ?>
  <?php if (isset($content['field_youtube_url'])) { ?>
    <li><a target="_blank" href="<?php print render($content['field_youtube_url']); ?>"><i class="fa fa-youtube"></i></a></li>
  <?php } ?>
</ul>
<div class="clearfix"></div>
<br>
  <?php if (isset($content['field_url'])) { ?>
<div class="button-wrapper margin-top clearfix text-center">
<a target="_blank" href="<?php print render($content['field_url']['#items']['0']['value']); ?>" class="btn btn-primary btn-lg margin-top">VISIT WEBSITE</a>
</div>
  <?php } ?>


</div>
</div>

    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      //print render($content);
    ?>

<?php print render($content['title']); ?>
  </div>

  <?php print render($content['links']); ?>

  <?php print render($content['comments']); ?>
</div>
</div>
